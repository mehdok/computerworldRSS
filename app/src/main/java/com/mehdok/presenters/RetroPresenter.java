package com.mehdok.presenters;

import com.mehdok.interfaces.ApiService;
import com.mehdok.models.Episode;
import com.mehdok.models.Serial;
import com.mehdok.models.Show;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by MEHDOK on 12/31/2015.
 */
public class RetroPresenter {

    public static final String BASE_URL = "http://api.tvmaze.com";
    private static RetroPresenter sInstance;
    private final ApiService mApiService;

    public static RetroPresenter getInstance() {
        if (sInstance == null) {
            sInstance = new RetroPresenter();
        }

        return sInstance;
    }

    // A private constructor is the correct way to do the singleton pattern

    /**
     * Construct a new RetroPresenter
     */
    private RetroPresenter() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mApiService = retrofit.create(ApiService.class);
    }

    /**
     * Gets serial list.
     * <p/>
     * We need this call object which returns from retrofit we declare this in {@link ApiService#getSchedule(String)}
     *
     * @return the serial list
     */
    public Call<List<Serial>> getSerialList() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        String format = simpleDateFormat.format(new Date());
        return mApiService.getSchedule(format);
    }

    /**
     * Get show call.
     *
     * @param id the id od show
     * @return the call
     */
    public Call<Show> getShow(String id){
        return mApiService.getShow(id);
    }

    /**
     * Get episode call.
     *
     * @param id the id of episode
     * @return the call
     */
    public Call<Episode> getEpisode(String id){
        return mApiService.getEpisode(id);
    }
}
