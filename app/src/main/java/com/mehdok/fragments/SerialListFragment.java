package com.mehdok.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.mehdok.adapters.SerialListAdapter;
import com.mehdok.computerworldrss.MainActivity;
import com.mehdok.computerworldrss.R;
import com.mehdok.holders.SerialsListHolder;
import com.mehdok.models.Serial;
import com.mehdok.views.VerticalSpaceItemDecoration;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class SerialListFragment extends Fragment implements Callback<List<Serial>>
{
    //TODO show progress bar

    private RecyclerView mRecyclerView;
    private SerialListAdapter mAdapter;
    private List<Serial> mSerialsList;
    private LinearLayout baseLayout;
    private ProgressBar mProgressBar;

    public SerialListFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_serial_list, container, false);

        findViews(v);
        if (savedInstanceState != null && SerialsListHolder.getSerialsList() != null)
        {
            mSerialsList = SerialsListHolder.getSerialsList();
            fillViews(mSerialsList);
        }
        else
        {
            getSerialsList();
        }

        return v;
    }

    private void findViews(View v)
    {
        mRecyclerView = (RecyclerView) v.findViewById(R.id.serials_recycler_view);
        baseLayout = (LinearLayout)v.findViewById(R.id.serial_list_base);
        mProgressBar = (ProgressBar)v.findViewById(R.id.serial_list_progress_bar);
    }

    private void getSerialsList()
    {
        if (mSerialsList == null)
        {
            showProgressBar(true);
            //RetroPresenter.getInstance().getSerialList().enqueue(this);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            String format = simpleDateFormat.format(new Date());
            ((MainActivity)getActivity()).getApiService().getSchedule(format).enqueue(this);
        }
        else
            fillViews(mSerialsList);
    }

    private void fillViews(List<Serial> serial)
    {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(getActivity(), R.drawable.divider));
        mAdapter = new SerialListAdapter(getActivity(), serial);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onResponse(Response<List<Serial>> response, Retrofit retrofit)
    {
        showProgressBar(false);
        mSerialsList = response.body();
        fillViews(mSerialsList);
    }

    @Override
    public void onFailure(Throwable t)
    {
        Log.w("Callback", "onFailure", t);
        Snackbar.make(baseLayout, R.string.serial_list_error, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onSaveInstanceState(Bundle state)
    {
        super.onSaveInstanceState(state);
        SerialsListHolder.setSerialsList(mSerialsList);
    }

    public void showProgressBar(boolean show)
    {
        if (show)
        {
            mProgressBar.setVisibility(View.VISIBLE);
        }
        else
        {
            mProgressBar.setVisibility(View.GONE);
        }
    }
}
