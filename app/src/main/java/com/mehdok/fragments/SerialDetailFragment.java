package com.mehdok.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mehdok.computerworldrss.MainActivity;
import com.mehdok.computerworldrss.R;
import com.mehdok.models.Show;
import com.mehdok.presenters.RetroPresenter;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class SerialDetailFragment extends Fragment
{
    private Show mShow;
    private String mShowId;

    public SerialDetailFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_serial_detail, container, false);
        getExtras();
        final ImageView imageView = (ImageView) v.findViewById(R.id.imageView_show);
        final TextView textViewTitle = (TextView) v.findViewById(R.id.textView_title);
        final TextView textViewDesc = (TextView) v.findViewById(R.id.textView_desc);
        RetroPresenter.getInstance().getShow(mShowId).enqueue(new Callback<Show>() {
            @Override
            public void onResponse(Response<Show> response, Retrofit retrofit) {
                Glide.with(getActivity().getApplicationContext())
                        .load(response.body().getImage().getOriginal())
                        .into(imageView);
                textViewTitle.setText(response.body().getName());
                textViewDesc.setText(Html.fromHtml(response.body().getSummary()));
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
        return v;
    }

    private void getExtras()
    {
        mShowId = getArguments().getString(MainActivity.SHOW);
    }
}
