package com.mehdok.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mehdok.computerworldrss.R;
import com.mehdok.interfaces.SerialItemClickInterface;
import com.mehdok.models.Image;
import com.mehdok.models.Serial;

import java.util.List;

/**
 * Created by MEHDOK on 12/27/2015.
 */
public class SerialListAdapter extends RecyclerView.Adapter<SerialListAdapter.ItemViewHolder>
{
    private Context mContext;
    private List<Serial> mSerials;
    private SerialItemClickInterface mClickCallback;

    public SerialListAdapter(Context context, List<Serial> serials)
    {
        mContext = context;
        mSerials = serials;

        if (mContext instanceof SerialItemClickInterface)
            mClickCallback = (SerialItemClickInterface) mContext;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_serial_layout, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position)
    {
        final Serial serial = mSerials.get(position);

        Image image = serial.getShow().getImage();
        if (image != null)
        {
            Glide.with(mContext)
                    .load(image.getMedium())
                    .into(holder.serialPoster);
        }
        
        holder.serialName.setText(serial.getShow().getName());
        holder.serialSE.setText("S" + serial.getSeason() + "E" + mSerials.get(position).getNumber());
        holder.serialInfo.setText(Html.fromHtml(serial.getShow().getSummary()));
        holder.serialDate.setText(serial.getAirdate() + "\n" + serial.getAirtime());

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (mClickCallback != null)
                {
                    mClickCallback.onItemClick(serial.getShow().getId().toString());
                }
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return mSerials.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView serialPoster;
        public TextView serialName;
        public TextView serialSE;
        public TextView serialInfo;
        public TextView serialDate;

        public ItemViewHolder(View view)
        {
            super(view);

            serialPoster = (ImageView)view.findViewById(R.id.serial_poster);
            serialName = (TextView)view.findViewById(R.id.serial_name);
            serialSE = (TextView)view.findViewById(R.id.serial_se);
            serialInfo = (TextView)view.findViewById(R.id.serial_info);
            serialDate = (TextView)view.findViewById(R.id.serial_date);
        }
    }
}
