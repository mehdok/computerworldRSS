package com.mehdok.computerworldrss;

import android.app.Application;

import com.mehdok.interfaces.NetComponent;
import com.mehdok.moduls.AppModule;
import com.mehdok.moduls.NetModule;

/**
 * Created by MEHDOK on 1/4/2016.
 */
public class App extends Application
{
    private NetComponent mNetComponent;

    @Override
    public void onCreate()
    {
        super.onCreate();

        mNetComponent = com.mehdok.interfaces.DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule("http://api.tvmaze.com"))
                .build();
    }

    public NetComponent getNetComponent()
    {
        return mNetComponent;
    }
}
