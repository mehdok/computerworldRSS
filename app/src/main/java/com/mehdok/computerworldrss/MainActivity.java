package com.mehdok.computerworldrss;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.mehdok.fragments.SerialDetailFragment;
import com.mehdok.fragments.SerialListFragment;
import com.mehdok.interfaces.ApiService;
import com.mehdok.interfaces.SerialItemClickInterface;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements SerialItemClickInterface
{
    public static final String DETAIL_URL = "detail_url";
    public static final String SHOW = "show";

    @Inject
    ApiService mApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ((App)getApplication()).getNetComponent().inject(this);

        if (savedInstanceState == null)
            init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void init()
    {
        getFragmentManager()
                .beginTransaction()
                .add(R.id.base_fragment_layout, new SerialListFragment())
                .commit();
    }

    @Override
    public void onItemClick(String id)
    {
        SerialDetailFragment detailFragment = new SerialDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString(SHOW, id);
        detailFragment.setArguments(bundle);

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.base_fragment_layout, detailFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed()
    {
        if(getFragmentManager().getBackStackEntryCount() > 0)
            getFragmentManager().popBackStack();
        else
            super.onBackPressed();
    }

    public ApiService getApiService()
    {
        return mApiService;
    }
}
