package com.mehdok.holders;

import com.mehdok.models.Serial;

import java.util.List;

/**
 * Created by MEHDOK on 12/30/2015.
 */
public class SerialsListHolder
{
    private static List<Serial> serialsList;

    public static List<Serial> getSerialsList()
    {
        return serialsList;
    }

    public static void setSerialsList(List<Serial> serialsList)
    {
        // this can be a hard copy, it wast memory but data is persistent as long as app is running
        SerialsListHolder.serialsList = serialsList;
    }
}
