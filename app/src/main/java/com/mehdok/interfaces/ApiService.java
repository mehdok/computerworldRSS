package com.mehdok.interfaces;

import com.mehdok.models.Episode;
import com.mehdok.models.Serial;
import com.mehdok.models.Show;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by MEHDOK on 12/27/2015.
 */
public interface ApiService
{
    /**
     * Gets schedule list for the given date
     *
     * @param date the date
     * @return the schedule
     */
    @GET("/schedule?country=US")
    Call<List<Serial>> getSchedule(@Query("date")String date);

    /**
     * Gets a show by it's id
     *
     * @param id the id
     * @return the show
     */
    @GET("/shows/{id}")
    Call<Show> getShow(@Path("id") String id);

    /**
     * Gets an episode by it's detail
     *
     * @param id the id
     * @return the episode
     */
    @GET("/episodes/{id}")
    Call<Episode> getEpisode(@Path("id") String id);
}
