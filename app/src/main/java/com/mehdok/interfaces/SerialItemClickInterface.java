package com.mehdok.interfaces;

import com.mehdok.models.Show;

/**
 * Created by MEHDOK on 12/30/2015.
 */
public interface SerialItemClickInterface
{
    void onItemClick(String id);
}
