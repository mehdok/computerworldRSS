package com.mehdok.interfaces;

import com.mehdok.computerworldrss.MainActivity;
import com.mehdok.moduls.AppModule;
import com.mehdok.moduls.NetModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by MEHDOK on 1/4/2016.
 */

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent
{
    void inject(MainActivity activity);
}
