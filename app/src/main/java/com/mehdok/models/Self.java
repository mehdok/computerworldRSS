
package com.mehdok.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Self implements Parcelable{

    @SerializedName("href")
    @Expose
    private String href;

    /**
     * 
     * @return
     *     The href
     */
    public String getHref() {
        return href;
    }

    /**
     * 
     * @param href
     *     The href
     */
    public void setHref(String href) {
        this.href = href;
    }

    private Self(Parcel in)
    {
        href = in.readString();
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(href);
    }

    public static final Parcelable.Creator<Self> CREATOR = new Parcelable.Creator<Self>()
    {
        public Self createFromParcel(Parcel in)
        {
            return new Self(in);
        }

        public Self[] newArray(int size)
        {
            return new Self[size];
        }
    };
}
