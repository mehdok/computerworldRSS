
package com.mehdok.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Schedule implements Parcelable
{
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("days")
    @Expose
    private List<String> days = new ArrayList<String>();

    /**
     * 
     * @return
     *     The time
     */
    public String getTime() {
        return time;
    }

    /**
     * 
     * @param time
     *     The time
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * 
     * @return
     *     The days
     */
    public List<String> getDays() {
        return days;
    }

    /**
     * 
     * @param days
     *     The days
     */
    public void setDays(List<String> days) {
        this.days = days;
    }

    private Schedule(Parcel in)
    {
        time = in.readString();
        days = new ArrayList<String>();
        in.readStringList(days);
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(time);
        dest.writeStringList(days);
    }

    public static final Creator<Schedule> CREATOR = new Creator<Schedule>()
    {
        public Schedule createFromParcel(Parcel in)
        {
            return new Schedule(in);
        }

        public Schedule[] newArray(int size)
        {
            return new Schedule[size];
        }
    };

}
