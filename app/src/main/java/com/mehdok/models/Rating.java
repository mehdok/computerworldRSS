
package com.mehdok.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rating implements Parcelable{

    @SerializedName("average")
    @Expose
    private Object average;

    /**
     * 
     * @return
     *     The average
     */
    public Object getAverage() {
        return average;
    }

    /**
     * 
     * @param average
     *     The average
     */
    public void setAverage(Object average) {
        this.average = average;
    }

    private Rating(Parcel in)
    {
        average = in.readString();
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        if (average != null)
            dest.writeString(average.toString());
        else
            dest.writeString("");
    }

    public static final Creator<Rating> CREATOR = new Creator<Rating>()
    {
        public Rating createFromParcel(Parcel in)
        {
            return new Rating(in);
        }

        public Rating[] newArray(int size)
        {
            return new Rating[size];
        }
    };
}
