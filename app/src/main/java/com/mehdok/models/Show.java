
package com.mehdok.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Show implements Parcelable{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("genres")
    @Expose
    private List<Object> genres = new ArrayList<Object>();
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("runtime")
    @Expose
    private Integer runtime;
    @SerializedName("premiered")
    @Expose
    private String premiered;
    @SerializedName("schedule")
    @Expose
    private Schedule schedule;
    @SerializedName("rating")
    @Expose
    private Rating rating;
    @SerializedName("weight")
    @Expose
    private Integer weight;
    @SerializedName("network")
    @Expose
    private Network network;
    @SerializedName("webChannel")
    @Expose
    private Object webChannel;
    @SerializedName("externals")
    @Expose
    private Externals externals;
    @SerializedName("image")
    @Expose
    private Image image;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("updated")
    @Expose
    private Integer updated;
    @SerializedName("_links")
    @Expose
    private Links Links;

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * 
     * @param url
     *     The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * 
     * @param language
     *     The language
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * 
     * @return
     *     The genres
     */
    public List<Object> getGenres() {
        return genres;
    }

    /**
     * 
     * @param genres
     *     The genres
     */
    public void setGenres(List<Object> genres) {
        this.genres = genres;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The runtime
     */
    public Integer getRuntime() {
        return runtime;
    }

    /**
     * 
     * @param runtime
     *     The runtime
     */
    public void setRuntime(Integer runtime) {
        this.runtime = runtime;
    }

    /**
     * 
     * @return
     *     The premiered
     */
    public String getPremiered() {
        return premiered;
    }

    /**
     * 
     * @param premiered
     *     The premiered
     */
    public void setPremiered(String premiered) {
        this.premiered = premiered;
    }

    /**
     * 
     * @return
     *     The schedule
     */
    public Schedule getSchedule() {
        return schedule;
    }

    /**
     * 
     * @param schedule
     *     The schedule
     */
    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    /**
     * 
     * @return
     *     The rating
     */
    public Rating getRating() {
        return rating;
    }

    /**
     * 
     * @param rating
     *     The rating
     */
    public void setRating(Rating rating) {
        this.rating = rating;
    }

    /**
     * 
     * @return
     *     The weight
     */
    public Integer getWeight() {
        return weight;
    }

    /**
     * 
     * @param weight
     *     The weight
     */
    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    /**
     * 
     * @return
     *     The network
     */
    public Network getNetwork() {
        return network;
    }

    /**
     * 
     * @param network
     *     The network
     */
    public void setNetwork(Network network) {
        this.network = network;
    }

    /**
     * 
     * @return
     *     The webChannel
     */
    public Object getWebChannel() {
        return webChannel;
    }

    /**
     * 
     * @param webChannel
     *     The webChannel
     */
    public void setWebChannel(Object webChannel) {
        this.webChannel = webChannel;
    }

    /**
     * 
     * @return
     *     The externals
     */
    public Externals getExternals() {
        return externals;
    }

    /**
     * 
     * @param externals
     *     The externals
     */
    public void setExternals(Externals externals) {
        this.externals = externals;
    }

    /**
     * 
     * @return
     *     The image
     */
    public Image getImage() {
        return image;
    }

    /**
     * 
     * @param image
     *     The image
     */
    public void setImage(Image image) {
        this.image = image;
    }

    /**
     * 
     * @return
     *     The summary
     */
    public String getSummary() {
        return summary;
    }

    /**
     * 
     * @param summary
     *     The summary
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     * 
     * @return
     *     The updated
     */
    public Integer getUpdated() {
        return updated;
    }

    /**
     * 
     * @param updated
     *     The updated
     */
    public void setUpdated(Integer updated) {
        this.updated = updated;
    }

    /**
     * 
     * @return
     *     The Links
     */
    public Links getLinks() {
        return Links;
    }

    /**
     * 
     * @param Links
     *     The _links
     */
    public void setLinks(Links Links) {
        this.Links = Links;
    }

    private Show(Parcel in)
    {
        id = in.readInt();
        url = in.readString();
        name = in.readString();
        type = in.readString();
        language = in.readString();
        genres = new ArrayList<>();
        in.readList(genres, Object.class.getClassLoader());
        status = in.readString();
        runtime = in.readInt();
        premiered = in.readString();
        schedule = in.readParcelable(Schedule.class.getClassLoader());
        rating = in.readParcelable(Rating.class.getClassLoader());
        weight = in.readInt();
        network = in.readParcelable(Network.class.getClassLoader());
        //webChannel = in.readParcelable(Object.class.getClassLoader());
        externals = in.readParcelable(Externals.class.getClassLoader());
        image = in.readParcelable(Image.class.getClassLoader());
        summary = in.readString();
        updated = in.readInt();
        this.Links = in.readParcelable(com.mehdok.models.Links.class.getClassLoader());
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(id);
        dest.writeString(url);
        dest.writeString(name);
        dest.writeString(type);
        dest.writeString(language);
        dest.writeList(genres);
        dest.writeString(status);
        dest.writeInt(runtime);
        dest.writeString(premiered);
        dest.writeParcelable(schedule, flags);
        dest.writeParcelable(rating, flags);
        dest.writeInt(weight);
        dest.writeParcelable(network, flags);
        //dest.writeParcelable(webChannel, flags);
        dest.writeParcelable(externals, flags);
        dest.writeParcelable(image, flags);
        dest.writeString(summary);
        dest.writeInt(updated);
        dest.writeParcelable(this.Links, flags);
    }

    public static final Parcelable.Creator<Show> CREATOR = new Parcelable.Creator<Show>()
    {
        public Show createFromParcel(Parcel in)
        {
            return new Show(in);
        }

        public Show[] newArray(int size)
        {
            return new Show[size];
        }
    };
}
