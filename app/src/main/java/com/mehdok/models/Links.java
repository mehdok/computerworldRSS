
package com.mehdok.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Links  implements Parcelable{

    @SerializedName("self")
    @Expose
    private Self self;
    @SerializedName("previousepisode")
    @Expose
    private Previousepisode previousepisode;

    /**
     * 
     * @return
     *     The self
     */
    public Self getSelf() {
        return self;
    }

    /**
     * 
     * @param self
     *     The self
     */
    public void setSelf(Self self) {
        this.self = self;
    }

    /**
     * 
     * @return
     *     The previousepisode
     */
    public Previousepisode getPreviousepisode() {
        return previousepisode;
    }

    /**
     * 
     * @param previousepisode
     *     The previousepisode
     */
    public void setPreviousepisode(Previousepisode previousepisode) {
        this.previousepisode = previousepisode;
    }

    private Links(Parcel in)
    {
        self = in.readParcelable(Self.class.getClassLoader());
        previousepisode = in.readParcelable(Previousepisode.class.getClassLoader());
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeParcelable(self, flags);
        dest.writeParcelable(previousepisode, flags);
    }

    public static final Creator<Links> CREATOR = new Creator<Links>()
    {
        public Links createFromParcel(Parcel in)
        {
            return new Links(in);
        }

        public Links[] newArray(int size)
        {
            return new Links[size];
        }
    };

}
