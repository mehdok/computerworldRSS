
package com.mehdok.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Externals implements Parcelable {

    @SerializedName("tvrage")
    @Expose
    private Integer tvrage;
    @SerializedName("thetvdb")
    @Expose
    private Integer thetvdb;

    /**
     * 
     * @return
     *     The tvrage
     */
    public Integer getTvrage() {
        return tvrage;
    }

    /**
     * 
     * @param tvrage
     *     The tvrage
     */
    public void setTvrage(Integer tvrage) {
        this.tvrage = tvrage;
    }

    /**
     * 
     * @return
     *     The thetvdb
     */
    public Integer getThetvdb() {
        return thetvdb;
    }

    /**
     * 
     * @param thetvdb
     *     The thetvdb
     */
    public void setThetvdb(Integer thetvdb) {
        this.thetvdb = thetvdb;
    }

    private Externals(Parcel in)
    {
        tvrage = in.readInt();
        thetvdb = in.readInt();
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(tvrage);
        dest.writeInt(thetvdb);
    }

    public static final Parcelable.Creator<Externals> CREATOR = new Parcelable.Creator<Externals>()
    {
        public Externals createFromParcel(Parcel in)
        {
            return new Externals(in);
        }

        public Externals[] newArray(int size)
        {
            return new Externals[size];
        }
    };
}
