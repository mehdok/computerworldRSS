package com.mehdok.models;


import com.google.gson.annotations.SerializedName;

/**
 * @author : Pedramrn@gmail.com
 *         Created on: 2015-10-28
 */

public class Episode {

    @SerializedName("id")
    public int id;
    @SerializedName("url")
    public String url;
    @SerializedName("name")
    public String name;
    @SerializedName("season")
    public Integer season;
    @SerializedName("number")
    public Integer number;
    @SerializedName("airdate")
    public String airdate;
    @SerializedName("airtime")
    public String airtime;
    @SerializedName("airstamp")
    public String airstamp;
    @SerializedName("runtime")
    public Integer runtime;
    @SerializedName("image")
    public Image image;
    @SerializedName("summary")
    public String summary;
    @SerializedName("_links")
    public Links_ links;

}
