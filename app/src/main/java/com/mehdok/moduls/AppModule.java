package com.mehdok.moduls;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by MEHDOK on 1/4/2016.
 */

@Module
public class AppModule
{
    Application mApplication;

    public AppModule(Application application)
    {
        mApplication = application;
    }

    @Provides
    @Singleton
    public Application provideApplication()
    {
        return mApplication;
    }
}
